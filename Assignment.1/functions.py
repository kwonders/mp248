import numpy
def gauss(x, a=1, b=0, c=1):
    """This is a Gaussian Function"""
    return a*numpy.exp(-((x-b)**2)/(2*(c**2)))