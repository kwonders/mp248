import numpy
import functions

def sym_gauss_int_sqr(xb, n=10):
    """This computes the square of the total area inside the interval""" 
    w = (xb+xb)/n
    tot = 0
    i = 0 
    for i in range(n):
        x = -xb + (i*w)
        h1 = functions.gauss(x, c=numpy.sqrt(1/2))
        h2 = functions.gauss((x+w), c=numpy.sqrt(1/2))
        aveh = (h1 + h2) / 2 
        areatrap = (aveh*w)
        tot = tot + areatrap 
    totsqr = tot**2
    return (totsqr) 


  
    
